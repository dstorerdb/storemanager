package org.dstorerdb.storemanager.store;///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.dataStorManager.store;
//
//import java.io.File;
//import java.util.Collection;
//import java.util.function.Predicate;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.naming.OperationNotSupportedException;
//import org.dataStorManager.interfaces.ChangeListener;
//import org.martin.electroList.structure.ElectroList;
//
///**
// *
// * @author martin
// */
//public class DataList extends ElectroList {
//
//     // Considerar que en el listener al limpiar se debe dejar el nombre de la lista
//    // y ademas en lo posible tener la opcion de encriptar el contenido
//    
//    private final File fileList;
//    
//    private ChangeListener changeListener;
//
//    public DataList(String name, Collection col) {
//        this(name);
//        addAll(col);
//    }
//
//    public DataList(String name, Object[] array) {
//        this(name);
//        addAll(array);
//    }
//
//    public DataList(String name) {
//        super(name);
//    }
//
//    public boolean isNumber(String strObj){
//        
//    }
//    
//    public ChangeListener getChangeListener() {
//        return changeListener;
//    }
//
//    public void setChangeListener(ChangeListener changeListener) {
//        this.changeListener = changeListener;
//    }
//    
//    @Override
//    public void setName(String name) {
//        super.setName(name); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public boolean add(Object obj){
//        boolean add = super.add(obj);
//        changeListener.onAddObjectEvent(obj);
//        return add;
//    }
//    
//    @Override
//    public void add(int index, Object obj){
//        super.add(index, obj);
//        changeListener.onAddObjectEvent(obj, index);
//    }
//    
//    public Object removeFirst(Predicate predicate) {
//        Object remove = super.removeFirst(predicate); //To change body of generated methods, choose Tools | Templates.
//        if (remove != null)
//            changeListener.onRemoveObjectEvent(0);
//        
//        return remove;
//    }
//
//    @Override
//    public Object removeLast(Predicate predicate) {
//        Object remove = super.removeLast(predicate); //To change body of generated methods, choose Tools | Templates.
//        if (remove != null)
//            changeListener.onRemoveObjectEvent(size()-1);
//        return remove;
//    }
//
//    @Override
//    public boolean removeIf(Predicate filter) {
//        boolean remove = super.removeIf(filter); //To change body of generated methods, choose Tools | Templates.
//        if (remove) {
//            changeListener.onClearListEvent();
//            changeListener.onAddObjectsEvent(this);
//        }
//        return remove;
//    }
//
//    @Override
//    public boolean retainIf(Predicate filter) {
//        boolean retain = super.retainIf(filter); //To change body of generated methods, choose Tools | Templates.
//        if (retain) {
//            changeListener.onClearListEvent();
//            changeListener.onAddObjectEvent(this);
//        }
//        return retain;
//    }
//
//    @Override
//    public boolean remove(Object o) {
//        boolean remove = super.remove(o); //To change body of generated methods, choose Tools | Templates.
//        if (remove) {
//            changeListener.onClearListEvent();
//            changeListener.onAddObjectsEvent(this);
//        }
//        return remove;
//    }
//
//    @Override
//    public boolean addAll(Collection c) {
//        boolean addAll = super.addAll(c); //To change body of generated methods, choose Tools | Templates.
//        if (addAll)
//            changeListener.onAddObjectsEvent(c);
//        return addAll;
//    }
//
//    @Override
//    public boolean addAll(int index, Collection c) {
//        boolean addAll = super.addAll(index, c);
//        if (addAll)
//            changeListener.onAddObjectsEvent(index, c);
//        return addAll;
//    }
//
//    @Override
//    public void addAll(Object[] array) {
//        super.addAll(array); 
//        changeListener.onAddObjectsEvent(array);
//    }
//
//    @Override
//    public void addAll(Object[] array, int index) {
//        super.addAll(array, index); //To change body of generated methods, choose Tools | Templates.
//        changeListener.onAddObjectsEvent(array, index);
//    }
//
//    @Override
//    public boolean removeAll(Collection c) {
//        try {
//            throw new OperationNotSupportedException("Operacion aun no implementada fisicamente");
//        } catch (OperationNotSupportedException ex) {
//            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//
//    @Override
//    public boolean retainAll(Collection c) {
//        try {
//            throw new OperationNotSupportedException("Operacion aun no soportada fisicamente");
//        } catch (OperationNotSupportedException ex) {
//            Logger.getLogger(DataList.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
//
//    @Override
//    public void clear() {
//        super.clear(); //To change body of generated methods, choose Tools | Templates.
//        changeListener.onClearListEvent();
//    }
//
//    @Override
//    public Object set(int index, Object element) {
//        Object objSet = super.set(index, element);
//        if (objSet != null) {
//            changeListener.onSetObjectEvent(index, element);
//        }
//        return objSet;
//    }
//
//    @Override
//    public Object remove(int index) {
//        Object remove = super.remove(index);
//        if (remove != null)
//            changeListener.onRemoveObjectEvent(index);
//        return remove;
//    }
//
//    @Override
//    public void addFirst(Object obj) {
//        super.addFirst(obj); //To change body of generated methods, choose Tools | Templates.
//        changeListener.onAddObjectEvent(obj, 0);
//    }
//
//    @Override
//    public void addLast(Object obj) {
//        super.addLast(obj); //To change body of generated methods, choose Tools | Templates.
//        changeListener.onAddObjectEvent(obj);
//    }
//
//    @Override
//    public Object removeFirst() {
//        Object remove = super.removeFirst();
//        if (remove != null)
//            changeListener.onRemoveObjectEvent(0);
//        return remove;
//    }
//
//    @Override
//    public Object removeLast() {
//        Object remove = super.removeLast();
//        if (remove != null)
//            changeListener.onRemoveObjectEvent(size()-1);
//        return remove;
//    }
//
//    @Override
//    public Object pollFirst() {
//        return removeFirst();
//    }
//
//    @Override
//    public Object pollLast() {
//        return removeLast();
//    }
//
//    @Override
//    public boolean removeFirstOccurrence(Object o) {
//        return remove(o);
//    }
//
//    @Override
//    public boolean removeLastOccurrence(Object o) {
//        boolean remove = super.removeLastOccurrence(o);
//        if (remove) {
//            changeListener.onClearListEvent();
//            changeListener.onAddObjectsEvent(this);
//        }
//        return remove;
//    }
//
//    @Override
//    public Object remove() {
//        return removeFirst();
//    }
//
//    @Override
//    public Object poll() {
//        return removeFirst();
//    }
//
//    @Override
//    public void push(Object e) {
//        add(0, e);
//    }
//
//    @Override
//    public Object pop() {
//        return removeFirst();
//    }
//
//    @Override
//    public String toString() {
//        StringBuilder sBList = new StringBuilder();
//        
//    }
//
//}
