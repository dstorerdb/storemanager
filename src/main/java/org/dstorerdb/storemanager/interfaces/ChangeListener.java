/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.storemanager.interfaces;

import java.util.Collection;

/**
 *
 * @author martin
 */
public interface ChangeListener {
    public void onAddObjectEvent(Object obj);
    public void onAddObjectEvent(Object obj, int index);
    public void onAddObjectsEvent(Collection col);
    public void onAddObjectsEvent(int index, Collection c);
    public void onAddObjectsEvent(Object[] array);
    public void onAddObjectsEvent(Object[] array, int index);
    public void onRemoveObjectEvent(Object obj);
    public void onRemoveObjectEvent(int index);
    public void onClearListEvent();
    public void onSetObjectEvent(int index, Object obj);
}
